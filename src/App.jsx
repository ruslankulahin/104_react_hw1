import React from 'react';
import Button from './components/Button/Button.jsx';
import Modal from './components/Modal/Modal.jsx';
import './App.css';

export default class App extends React.Component {
  state = {
    isFirstModal: false,
    isSecondModal: false,
    isGreatModal: false,
    isFineModal: false,
    isSoSoModal: false
  };

  openFirstModal = () => {
    this.setState({ isFirstModal: true });
  };

  openSecondModal = () => {
    this.setState({ isSecondModal: true });
  };

  openGreatModal = () => {
    this.setState({ isGreatModal: true });
  }
  openFineModal = () => {
    this.setState({ isFineModal: true });
  }
  openSoSoModal = () => {
    this.setState({ isSoSoModal: true });
  }

  closeModal = () => {
    this.setState({ 
      isFirstModal: false,
      isSecondModal: false,
      isGreatModal: false,
      isFineModal: false,
      isSoSoModal: false
    });
  };

  handleOutside = (event) => {
    if (event.currentTarget === event.target) {
      this.closeModal();
    }
  };


  render() {
    return (
      <div className="btn-wrapper" >
        <Button
          optionalСlassName="btn-primary"
          backgroundColor="yellow"
          text="Open first modal"
          onClick={this.openFirstModal}
        />
        <Button
          optionalСlassName="btn-secondary"
          backgroundColor="blue"
          text="Open second modal"
          onClick={this.openSecondModal}
        />

        {this.state.isFirstModal && (
          <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutside={this.handleOutside}
            text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
            actions={
              <div className="button-container">
                  <Button 
                  className="btn" 
                  onClick={this.closeModal} 
                  text="Ok" 
                  />
                  <Button 
                  className="btn" 
                  onClick={this.closeModal}
                  text="Cancel"
                  />
              </div>
            }
          />
        )}

        {this.state.isSecondModal && (
          <Modal
            header="Home Work 1 - MODAL-WINDOW"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutside={this.handleOutside}
            text="How would you rate my work?"
            actions={
              <div className="button-container">
                <Button 
                  optionalСlassName="btn-primary" 
                  backgroundColor="yellow"
                  onClick={this.openGreatModal}
                  text="GREAT!"
                />
                <Button
                  optionalСlassName="btn-secondary" 
                  backgroundColor="blue" 
                  onClick={this.openFineModal}
                  text="Fine"
                />
                <Button 
                  onClick={this.openSoSoModal}
                  text="so-so"
                />
              </div>
            }
          />
        )}
        {this.state.isGreatModal && (
          <Modal
            header="THANKS for the HIGH RATING!"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutside={this.handleOutside}
            pathImage="https://cookbros.org/wp-content/uploads/2020/02/bigstock-Five-Star-Rating-Icon-evaluati-314731723.jpg"
            descrRaiting="Five-Star rating-icon"
        
          />
        )}
        {this.state.isFineModal && (
          <Modal
            header="It's better than 'so-so'"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutside={this.handleOutside}
            pathImage="../public/img/251.jpeg"
            descrRaiting="Thanks image"
        
          />
        )}
        {this.state.isSoSoModal && (
          <Modal
            header="Thank you anyway"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutside={this.handleOutside}
            pathImage="../public/img/62fa192ca6541673099853.png"
            descrRaiting="No-problem icon"
        
          />
        )}
      </div>
    );
  }
}

