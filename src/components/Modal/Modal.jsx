import React from "react";
import "./Modal.scss";

export default class Modal extends React.Component {
   
    render() {
        const { header, closeButton, text, actions, closeModal, handleOutside, pathImage, descrRaiting } = this.props;
        return (
        <div className="modal-wrapper" onClick={handleOutside}>
            <div className="modal">
                <div className="modal-content">
                    <div className="modal-header">
                        <h2>{header}</h2>
                        {closeButton && (
                        <span className="modal-close" onClick={closeModal}>
                            &times;
                        </span>
                        )}
                    </div>
                    <div className="modal-body">
                        <img className="modal-body_background" src={pathImage} alt={descrRaiting} />
                        <p className="body-text">{text}</p>
                    </div>
                    <div className="modal-footer">{actions}</div>
                </div>
            </div>
        </div>);
    }
}